//
//  MMDynamicCollectionViewController.h
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMDynamicCollectionViewController : UICollectionViewController

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@end
