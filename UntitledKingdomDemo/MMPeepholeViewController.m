//
//  MMPeepholeViewController.m
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

#import "MMPeepholeViewController.h"

@interface MMPeepholeViewController ()

@property (strong) CAShapeLayer *circleMask;
@property (assign) CGPoint circleOrigin;
@property (assign) CGFloat circleDiameter;

@property BOOL isMaskMoved;

@end

@implementation MMPeepholeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self startCircleMasking];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - circleMask

-(void)startCircleMasking
{
    self.circleOrigin = self.backgroundImageView.center;
    self.circleDiameter = 50.0f;
    
    self.circleMask = [CAShapeLayer layer];
    
    self.circleMask.actions = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNull null], @"path", nil];
    self.circleMask.fillColor = [UIColor blackColor].CGColor;
    self.circleMask.fillRule = kCAFillRuleEvenOdd;
    self.circleMask.frame = self.backgroundImageView.bounds;
    self.circleMask.opacity = 1.0f;
    
    [self refreshMask];
    [self.backgroundImageView.layer addSublayer:self.circleMask];
    
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [self.backgroundImageView addGestureRecognizer:panGesture];
    
    UISwipeGestureRecognizer *swipeGestureUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeUp:)];
    [swipeGestureUp setNumberOfTouchesRequired:2];
    [swipeGestureUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [self.backgroundImageView addGestureRecognizer:swipeGestureUp];
    
    UISwipeGestureRecognizer *swipeGestureDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeDown:)];
    [swipeGestureDown setNumberOfTouchesRequired:2];
    [swipeGestureDown setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.backgroundImageView addGestureRecognizer:swipeGestureDown];
    
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.backgroundImageView addGestureRecognizer:pinchGesture];
    
}

- (void)refreshMask
{
    CGFloat circleRadius = self.circleDiameter/2.0f;
    
    CGMutablePathRef newPath = CGPathCreateMutable();
    CGPathAddRect(newPath, NULL, self.backgroundImageView.bounds);
    CGPathAddEllipseInRect(newPath, NULL, CGRectMake(self.circleOrigin.x - circleRadius, self.circleOrigin.y - circleRadius, self.circleDiameter, self.circleDiameter));
    
    self.circleMask.path = newPath;
    
    CGPathRelease(newPath);
}

#pragma mark - handleGestures

- (void)handlePan:(UIPanGestureRecognizer *)sender
{
    if(self.circleMask.opacity == 1.0f)
    {
        self.circleOrigin = [sender locationInView:self.backgroundImageView];
        [self refreshMask];
    }
}

- (void)handleSwipeUp:(UISwipeGestureRecognizer *)sender
{
    [self fadeOut];
}

- (void)handleSwipeDown:(UISwipeGestureRecognizer *)sender
{
    [self fadeIn];
}

- (void)handlePinch:(UIPinchGestureRecognizer *)sender
{
    if(self.circleMask.opacity == 1.0f)
    {
        self.circleDiameter += sender.velocity;
        [self refreshMask];
    }
}

//-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
//{
//    return NO;
//}

- (void)fadeOut
{
    if(self.circleMask.opacity == 1.0f)
    {
        NSLog(@"odpalam fadeout");
        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [anim setFromValue: [NSNumber numberWithFloat:1.0f]];
        [anim setToValue:[NSNumber numberWithFloat:0.0f]];
        [anim setDelegate:self];
        [anim setDuration:0.25];

        [self.circleMask addAnimation:anim forKey:@"opacity"];
        self.circleMask.opacity = 0.0f;
    }
}

- (void)fadeIn
{
    if(self.circleMask.opacity == 0.0f)
    {
        NSLog(@"odpalam fadeIn");
        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [anim setFromValue: [NSNumber numberWithFloat:0.0f]];
        [anim setToValue:[NSNumber numberWithFloat:1.0f]];
        [anim setDelegate:self];
        [anim setDuration:0.25];

        [self.circleMask addAnimation:anim forKey:@"opacity"];
        self.circleMask.opacity = 1.0f;
    }
}

@end
