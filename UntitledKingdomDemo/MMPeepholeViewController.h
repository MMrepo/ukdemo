//
//  MMPeepholeViewController.h
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

//Drugi ekran:
//Ekran składa się z obrazka i nałożonego na nim widoku z przezroczystym otworem, przez który widać obraz. Poruszając palcem po ekranie zmieniamy położenie przezroczystego otworu. Gestem pinch zmieniamy średnicę otworu. Swipe dwoma palcami w górę odsłania cały obraz z animacją wygaszania (fadeOut). Swipe dwoma palcami w dół pojawia ponownie ekran z przezroczystym otworem.

#import <UIKit/UIKit.h>

@interface MMPeepholeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end
