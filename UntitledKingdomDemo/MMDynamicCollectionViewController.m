//
//  MMDynamicCollectionViewController.m
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

#import "MMDynamicCollectionViewController.h"
#import "MMDynamicCollectionViewFlowLayout.h"

@interface MMDynamicCollectionViewController () 

@property NSMutableArray * collectionItems;

@end

@implementation MMDynamicCollectionViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.collectionView setCollectionViewLayout:[[MMDynamicCollectionViewFlowLayout alloc] init]];
    
    self.collectionItems = [[NSMutableArray alloc] init];
    
    for (NSInteger i=0; i<150; ++i)
    {
        CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
        CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
        CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
        UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
        [self.collectionItems addObject:color];
    }
    
    
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];


    UIButton *aButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    CGPoint center = self.collectionView.center;
    aButton.frame = CGRectMake(center.x-50,center.y-15,100,30);
    [aButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [aButton setTitle:@"BOOM" forState:UIControlStateNormal];
    [aButton addTarget:self action:@selector(boomButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:aButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    MMDynamicCollectionViewFlowLayout *layout = (MMDynamicCollectionViewFlowLayout*) self.collectionView.collectionViewLayout;
    [layout resetLayout];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)boomButtonClicked
{
    MMDynamicCollectionViewFlowLayout *collectionViewLayout = (MMDynamicCollectionViewFlowLayout*)self.collectionView.collectionViewLayout;
    [collectionViewLayout turnOnGravity];
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.collectionItems count];
}

-(NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    cell.backgroundColor = [self.collectionItems objectAtIndex:indexPath.row];
    return cell;
}



@end
