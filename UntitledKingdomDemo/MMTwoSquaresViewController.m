//
//  MMViewController.m
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

#import "MMTwoSquaresViewController.h"

@interface MMTwoSquaresViewController () <UICollisionBehaviorDelegate>

@property UIDynamicAnimator* dynamicAnimator;
@property UICollisionBehavior* collisionBehavior;

@property UIAttachmentBehavior *touchAttachement;

@property CGPoint currentLocation;

@end

@implementation MMTwoSquaresViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.mainSquare.backgroundColor = [UIColor grayColor];
    self.smallSquare.backgroundColor = [UIColor redColor];
    
    [self startDynamicAnimations];
}

-(void)startDynamicAnimations
{
    self.dynamicAnimator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.collisionBehavior = [[UICollisionBehavior alloc] initWithItems:@[self.smallSquare, self.mainSquare]];
    self.collisionBehavior.collisionDelegate = self;
    self.collisionBehavior.translatesReferenceBoundsIntoBoundary = YES;
    
    UIOffset offset = UIOffsetMake(0, 0);
    UIOffset offset2 = UIOffsetMake(20, 20);
    UIAttachmentBehavior* attachement = [[UIAttachmentBehavior alloc] initWithItem:self.mainSquare offsetFromCenter:offset attachedToItem:self.smallSquare offsetFromCenter:offset2];
    
    [attachement setFrequency:2.0];
    [attachement setDamping:0.5];

    [self.dynamicAnimator addBehavior: attachement];
    [self.dynamicAnimator addBehavior: self.collisionBehavior];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - input

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *myTouch = [touches anyObject];
    self.currentLocation = [myTouch locationInView:self.view];
    // UIOffset offset = UIOffsetMake(20, 20);
    
    self.touchAttachement = [[UIAttachmentBehavior alloc] initWithItem:self.mainSquare
                        // offsetFromCenter:offset
                                                 attachedToAnchor:self.currentLocation];
    
    [self.dynamicAnimator addBehavior:self.touchAttachement];
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* myTouch = [touches anyObject];
    self.currentLocation = [myTouch locationInView:self.view];
    self.touchAttachement.anchorPoint = self.currentLocation;
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.dynamicAnimator removeBehavior:self.touchAttachement];
}


@end
