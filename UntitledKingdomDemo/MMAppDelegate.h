//
//  MMAppDelegate.h
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
