//
//  MMViewController.h
//  UntitledKingdomDemo
//
//  Created by Mateusz Małek on 12.01.2014.
//  Copyright (c) 2014 Mateusz Małek. All rights reserved.
//
//

//Pierwszy ekran:
//Ekran składa się z dwóch kwadratów. Poruszając palcem po ekranie przesuwamy jeden z nich, a drugi podąża za nim.


#import <UIKit/UIKit.h>

@interface MMTwoSquaresViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *mainSquare;
@property (weak, nonatomic) IBOutlet UIView *smallSquare;

@end
